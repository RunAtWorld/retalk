package main

import (
	"fmt"
	"io"
	"net"
	"retalk/common/message"
	"retalk/server/process"
	"retalk/server/utils"
)

//Processor：根据客户端发送信息不同，调用不同的函数
type Processor struct {
	Conn net.Conn //连接
}

//消息转发方法
func (this *Processor) ServerProcessMes(mes *message.Message) (err error) {

	//输出消息内容
	fmt.Println("mes=", mes)
	switch mes.Type {
	case message.LoginMesType:
		//处理登录
		userProcess := &process.UserProcess{
			Conn: this.Conn,
		}
		err = userProcess.ServerProcessLogin(mes)
	case message.RegisterMesType:
		//处理注册
		userProcess := &process.UserProcess{
			Conn: this.Conn,
		}
		err = userProcess.ServerProcessRegister(mes)
	case message.SmsMesType:
		//创建一个实例完成群消息转发
		smsProcess := &process.SmsProcess{}
		smsProcess.SendGroups(mes)
	default:
		fmt.Println("消息类型不存在，无法处理...")
	}
	return
}

//处理和客户端的通讯
func (this *Processor) ProcessReq(conn net.Conn) (err error) {
	//循环向客户端发送消息
	for {
		fmt.Println("读取客户端发送的数据...")
		//读取消息数据包
		transfer := &utils.Transfer{
			Conn: this.Conn,
		}
		var mes message.Message
		mes, err = transfer.ReadPkg()
		if err != nil {
			if err == io.EOF {
				fmt.Println("客户端退出，服务器端也退出...")
				return
			} else {
				fmt.Println("readPkg错误 err=", err)
				return
			}
		}
		fmt.Println("消息: mes=", mes)
		err = this.ServerProcessMes(&mes)
		if err != nil {
			fmt.Println("消息处理错误：err=", err)
			return
		}
	}

}
