package main

import (
	"time"

	"github.com/garyburd/redigo/redis"
)

//定义一个全局的Pool
var pool *redis.Pool

//初始化连接池
func InitPool(address string, maxIdle, maxActive int, idleTimeout time.Duration) {
	pool = &redis.Pool{
		MaxIdle:     maxIdle,     //最大空闲连接数
		MaxActive:   maxActive,   //和数据库的最大连接数，0表示没有限制
		IdleTimeout: idleTimeout, //连接最大空闲时间
		Dial: func() (redis.Conn, error) { //初始化连接代码，连接到哪个地址
			return redis.Dial("tcp", address)
		},
	}
}
