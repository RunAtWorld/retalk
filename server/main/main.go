package main

import (
	"fmt"
	"net"
	"retalk/server/model"
	"time"
)

//处理和客户端的通讯
func start_process(conn net.Conn) {
	fmt.Printf("开始和客户端 %v 通讯\n", conn.RemoteAddr())
	defer conn.Close()
	//调用转发主控
	processor := &Processor{
		Conn: conn,
	}
	err := processor.ProcessReq(conn)
	if err != nil {
		fmt.Println("处理和客户端通讯时错误，err=", err)
		return
	}
}

//完成对UserDao的初始化:InitPool要在initUserDao之前
func initUserDao() {
	model.MyUserDao = model.NewUserDao(pool)
}

func init() {
	//服务器启动时，初始化连接池
	InitPool("127.0.0.1:6379", 8, 0, 300*time.Second)
	initUserDao()
}
func main() {
	//开始监听服务器
	fmt.Println("服务器在8889端口开始监听...")
	listen, err := net.Listen("tcp", "127.0.0.1:8889")
	if err != nil {
		fmt.Println("net.listen err=", err)
		return
	}
	defer listen.Close()

	//等待客户端连接
	for {
		fmt.Println("等待客户端连接服务器...")
		conn, err := listen.Accept()
		if err != nil {
			fmt.Println("客户端连接失败 listen.Accept() err=", err)
		}

		//客户端连接成功后，开启一个协程和客户端保持通讯
		go start_process(conn)
	}
}
