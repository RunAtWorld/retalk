package process

import "fmt"

//UserMgr在服务器中只有一个，因此定义为全局变量
var (
	userMgr *UserMgr
)

type UserMgr struct {
	onlineUsers map[int]*UserProcess
}

//完成对userMgr的初始化
func init() {
	userMgr = &UserMgr{
		onlineUsers: make(map[int]*UserProcess, 1024),
	}
	fmt.Printf("完成对 %v 的初始化...\n", userMgr)
}

//完成对online User的添加
func (this *UserMgr) AddOnlineUser(userProcess *UserProcess) {
	this.onlineUsers[userProcess.UserId] = userProcess
}

//完成对online User的删除
func (this *UserMgr) DelOnlineUser(userId int) {
	delete(this.onlineUsers, userId)
}

//查询所有在线
func (this *UserMgr) GetAllOnlineUser() map[int]*UserProcess {
	return this.onlineUsers
}

//根据Id获取对应的值
func (this *UserMgr) GetOnlineUserById(userId int) (userProcess *UserProcess, err error) {
	userProcess, ok := this.onlineUsers[userId]
	if !ok {
		err = fmt.Errorf("用户 %d 不存在", userId)
		return
	}
	return
}
