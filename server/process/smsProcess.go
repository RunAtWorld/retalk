package process

import (
	"encoding/json"
	"fmt"
	"net"
	"retalk/common/message"
	"retalk/server/utils"
)

type SmsProcess struct{}

//群发消息
func (this *SmsProcess) SendGroups(mes *message.Message) {
	//遍历服务器端 onlineUsers map[int]*UserProcess，将消息转发出去

	//取出message的内容
	var smsMes message.SmsMes
	err := json.Unmarshal([]byte(mes.Data), &smsMes)
	if err != nil {
		fmt.Println("群发消息时 json.Unmarshal错误:err=", err)
		return
	}
	data, err := json.Marshal(mes)
	if err != nil {
		fmt.Println("群发消息时 json.Marshal错误:err=", err)
		return
	}
	for id, up := range userMgr.onlineUsers {
		if id == smsMes.UserId { //不发送给自己
			continue
		}
		this.SendMesToEachOnlineUser(data, up.Conn)
	}
}

//发送消息给其他用户
func (this *SmsProcess) SendMesToEachOnlineUser(data []byte, conn net.Conn) {
	//发送消息
	transfer := &utils.Transfer{
		Conn: conn,
	}
	err := transfer.WritePkg(data)
	if err != nil {
		fmt.Println("发送消息给其他用户 transfer.WritePkg错误:err=", err)
		return
	}
}
