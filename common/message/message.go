package message

import (
	"retalk/common/entity"
)

const (
	LoginMesType         = "LoginMes"
	LoginResMesType      = "LoginResMes"
	RegisterMesType      = "RegisterMesType"
	RegisterResMesType   = "RegisterResMesType"
	NotifyUserStatusType = "NotifyUserStatusType"
	SmsMesType           = "SmsMesType"
)

//用户状态的常量
const (
	UserOnline = iota
	UserOffLine
	UserBusy
)

type Message struct {
	Type string `json:"type"` //消息类型
	Data string `json:"data"` //消息数据
}

type LoginMes struct {
	UserId   int    `json:"userid"`   //用户id
	UserPwd  string `json:"userpwd"`  //用户密码
	UserName string `json:"username"` // 用户名
}

type LoginResMes struct {
	Code    int    `json:"code"` //返回的状态码 500:用户未注册 200:用户登录成功
	UserIds []int  //增加字段，保存用户id的切片
	Error   string `json:"error"` //返回错误信息
}

type RegisterMes struct {
	User entity.User `json:"user"` //User结构体
}

type RegisterResMes struct {
	Code  int    `json:"code"`  //返回状态码， 400表示改用户已注册，200表示注册成功
	Error string `json:"error"` //返回错误消息
}

type NotifyUserStatusMes struct {
	UserId int `json:"userid"` //用户id
	Status int `json:"status"` //用户状态
}

type SmsMes struct {
	Content string `json:"content"` //消息内容
	entity.User
}
