package entity

type User struct {
	UserId     int    `json:"userid"`     //用户id
	UserPwd    string `json:"userpwd"`    //用户密码
	UserName   string `json:"username"`   // 用户名
	UserStatus int    `json:"userStatus"` //用户状态:在线，离线
}
