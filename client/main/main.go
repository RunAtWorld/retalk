package main

import (
	"fmt"
	"retalk/client/process"
)

//定义用户的id和密码
var userid int
var userpwd string
var username string

func main() {
	fmt.Println("系统启动中...")

	var key int          // 接受用户输入
	var loop bool = true // 判断是否继续显示
	for loop {
		fmt.Println("-----------欢迎登录多人聊天室-----------")
		fmt.Printf("\t\t\t 1 登录聊天室\n")
		fmt.Printf("\t\t\t 2 注册用户\n")
		fmt.Printf("\t\t\t 3 退出聊天室\n")
		fmt.Printf("\t\t\t 请选择(1-3):\n")

		//登录界面
		fmt.Scanf("%d\n", &key)
		switch key {
		case 1:
			fmt.Println("登录聊天室")
			//用户准备登录,根据用户的输入，显示新的提示信息
			fmt.Println("请输入用户id:")
			fmt.Scanf("%d\n", &userid) //也可以写成	fmt.Scanln("%d", &userid)
			fmt.Println("请输入用户密码:")
			fmt.Scanf("%s\n", &userpwd)
			//登录函数
			userProcess := &process.UserProcess{}
			err := userProcess.Login(userid, userpwd)
			if err != nil {
				fmt.Println("用户登录时发生错误:", err)
			}
		case 2:
			fmt.Println("注册用户")
			fmt.Println("请输入用户id:")
			fmt.Scanf("%d\n", &userid) //也可以写成	fmt.Scanln("%d", &userid)
			fmt.Println("请输入用户密码:")
			fmt.Scanf("%s\n", &userpwd)
			fmt.Println("请输入用户昵称:")
			fmt.Scanf("%s\n", &username)
			//调用userProcess完成注册
			//登录函数
			userProcess := &process.UserProcess{}
			err := userProcess.Register(userid, userpwd, username)
			if err != nil {
				fmt.Println("用户注册时发生错误:", err)
			}
		case 3:
			fmt.Println("退出聊天室")
			loop = false
		default:
			fmt.Println("您的输入有误，请重新输入")
		}

	}
}
