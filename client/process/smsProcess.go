package process

import (
	"encoding/json"
	"fmt"
	"retalk/client/utils"
	"retalk/common/message"
)

type SmsProcess struct {
}

//群发消息
func (this *SmsProcess) SendGroupMes(content string) (err error) {
	//1.创建Mes
	var mes message.Message
	mes.Type = message.SmsMesType

	//2.创建SmsMes
	var smsMes message.SmsMes
	smsMes.Content = content
	smsMes.UserId = CurUser.UserId
	smsMes.UserStatus = CurUser.UserStatus

	//3. 序列化 SmsMes
	data, err := json.Marshal(smsMes)
	if err != nil {
		fmt.Println("群发消息 时 json.Marshal 发生错误:err=", err)
		return
	}
	mes.Data = string(data)

	//4. 对mes再次序列化
	data, err = json.Marshal(mes)
	if err != nil {
		fmt.Println("群发消息 时 json.Marshal 发生错误:err=", err)
		return
	}

	//5. 将mes发送给服务器
	transfer := utils.Transfer{
		Conn: CurUser.Conn,
	}

	//6. 发送
	err = transfer.WritePkg(data)
	if err != nil {
		fmt.Println("群发消息 时 transfer.WritePkg 发生错误:err=", err)
		return
	}
	return
}
