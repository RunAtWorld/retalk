package process

import (
	"encoding/json"
	"fmt"
	"net"
	"os"
	"retalk/client/utils"
	"retalk/common/message"
)

//显示菜单
func ShowMenu() {
	fmt.Println("-----恭喜用户登录成功-----")
	fmt.Println("-----1. 显示在线用户列表-----")
	fmt.Println("-----2. 发送消息-----")
	fmt.Println("-----3. 信息列表-----")
	fmt.Println("-----4. 退出系统-----")
	fmt.Println("请选择(1-4):")
	var key int
	fmt.Scanf("%d\n", &key)

	var content string
	smsProcess := &SmsProcess{}
	switch key {
	case 1:
		// fmt.Println("显示在线用户列表如下:")
		outputOnlineUser()
	case 2:
		fmt.Println("请输入群聊消息：")
		fmt.Scanf("%s\n", &content)
		smsProcess.SendGroupMes(content)
	case 3:
		fmt.Println("信息列表如下:")
	case 4:
		fmt.Println("你选择了退出了系统...")
		os.Exit(0)
	default:
		fmt.Println("输入的选项不正确...")
	}
}

//和服务器端保持通信
func ServerProcessMes(conn net.Conn) {
	//创建一个transfer实例
	transfer := utils.Transfer{
		Conn: conn,
	}
	for {
		fmt.Println("客户端正在等待服务器发送消息...")
		mes, err := transfer.ReadPkg()
		if err != nil {
			fmt.Println("读取服务器发送消息时错误,err=", err)
			return
		}
		fmt.Printf("读取到服务器消息：%v\n", mes)
		switch mes.Type {
		case message.NotifyUserStatusType: //有人上线
			//1.取出NotifyUserStatusMes
			var notifyUserStatusMes message.NotifyUserStatusMes
			err = json.Unmarshal([]byte(mes.Data), &notifyUserStatusMes)
			if err != nil {
				fmt.Println("和服务器端保持通信 json.Unmarshal 发生错误,err=", err)
				return
			}
			//2. 把上线用户的信息和状态保存到客户端维护的map中
			UpdateUserStatus(notifyUserStatusMes)
		case message.SmsMesType: //有人群发消息
			outputGroupMes(&mes)
		default:
			fmt.Println("服务器返回了未知消息类型！")
		}
	}

}
