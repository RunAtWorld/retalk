package process

import (
	"fmt"
	"retalk/client/model"
	"retalk/common/entity"
	"retalk/common/message"
)

//客户端要维护的onLineUsers
var onLineUsers map[int]*entity.User = make(map[int]*entity.User, 10)
var CurUser model.CurUser //用户登录成功后即完成对CurUser的初始化

//处理返回的NotifyUserStatusMes
func UpdateUserStatus(notifyUserStatusMes message.NotifyUserStatusMes) {
	//先判断用户之前是否已经上线过
	user, ok := onLineUsers[notifyUserStatusMes.UserId]
	if !ok { //如果之前map中没有该用户的上线信息,增加
		user = &entity.User{
			UserId:     notifyUserStatusMes.UserId,
			UserStatus: notifyUserStatusMes.Status,
		}
		onLineUsers[notifyUserStatusMes.UserId] = user
	}
	user.UserStatus = notifyUserStatusMes.Status
	onLineUsers[notifyUserStatusMes.UserId] = user
	outputOnlineUser()
}

//在客户端显示当前在线用户
func outputOnlineUser() {
	fmt.Println("当前在线用户列表如下:")
	fmt.Println("----------------------")
	for _, v := range onLineUsers {
		fmt.Printf("用户:%v\n", v.UserId)
	}
	fmt.Println("----------------------")
	fmt.Println("")
}
