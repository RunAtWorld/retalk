package process

import (
	"encoding/json"
	"fmt"
	"retalk/common/message"
)

//输出群发消息
func outputGroupMes(mes *message.Message) {
	//1. 反序列化mes
	var smsMes message.SmsMes
	err := json.Unmarshal([]byte(mes.Data), &smsMes)
	if err != nil {
		fmt.Println("输出群发消息时，json.Unmarshal发生错误：err=", err)
		return
	}

	//显示消息内容
	info := fmt.Sprintf("用户　[%d]　对大家说：\t%s", smsMes.UserId, smsMes.Content)
	fmt.Println(info)
}
