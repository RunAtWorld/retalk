package model

import (
	"net"
	"retalk/common/entity"
)

type CurUser struct {
	Conn net.Conn
	entity.User
}
